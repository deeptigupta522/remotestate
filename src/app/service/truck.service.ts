import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { truck } from '../model/truck.model';

@Injectable({
  providedIn: 'root'
})
export class TruckService {

  public api_url:string;

  constructor(private http: HttpClient ) {
    this.api_url = 'https://api.mystral.in/tt/mobile/logistics/searchTrucks?auth-company=PCH&companyId=33&deactivated=false&key=g2qb5jvucg7j8skpu5q7ria0mu&q-expand=true&q-include=lastRunningState,lastWaypoint';
   }

   getData(){
    return this.http.get<any>(this.api_url);
  }
}
