import { Component, OnInit } from '@angular/core';
import { truck } from 'src/app/model/truck.model';
import { TruckService } from 'src/app/service/truck.service';


@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.scss']
})
export class TopnavbarComponent implements OnInit {

  truckList : truck[]=[];
  totaltrucks= 0;
  runningtruck=0;
  stoppedtruck=0;
  idletruck=0;
  errortruck =0;

  constructor(private truckService : TruckService) { }

  ngOnInit(): void {
    this.TruckData();
  }

  TruckData(){
    this.truckService.getData().subscribe(data => {
      console.log('data',data)
      this.truckList =data.data;
      console.log(this.truckList);
      this.totaltrucks = this.truckList.length;

      this.runningtruck  = this.truckList.filter(obj=>
        obj.lastRunningState?.truckRunningState === 1
      ).length

      this.stoppedtruck  = this.truckList.filter(obj=>
        obj.lastRunningState?.truckRunningState === 0
      ).length

      this.idletruck  = this.truckList.filter(obj=>
        obj.lastRunningState?.truckRunningState === 0 && obj.lastWaypoint?.ignitionOn === true
      ).length

      this.errortruck = this.truckList.filter(obj =>{
        if(obj.lastRunningState?.stopStartTime){
          if((Date.now() - obj.lastRunningState?.stopStartTime ) > 4*60*60*1000){
            return true
          }
          return false
        }else{
          return false
        }}
      ).length
      
   })
  }

}
