import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { truck } from 'src/app/model/truck.model';
import { TruckService } from 'src/app/service/truck.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  private truckList :truck[]=[];
  private map: L.Map;
  private centroid: L.LatLngExpression = [30.753705978393555, 76.1063003540039]; //
  private aar:any =null;

  private initMap(): void {
    this.map = L.map('map', {
      center: this.centroid,
      zoom: 5
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    const jittery = this.aar.map(
        x => L.marker(x as L.LatLngExpression)
      ).forEach(
        x => x.addTo(this.map)
      );

    tiles.addTo(this.map);
  
  }

  constructor(private truckService : TruckService) { }

  ngOnInit(): void {
    this.truckService.getData().subscribe(data => {
      this.truckList =data.data;
      this.aar = this.truckList.map(a=>[a.lastWaypoint.lat, a.lastWaypoint.lng]);
    console.log('arrrray',this.aar);
    this.initMap();
    })
    
    
  }

}
