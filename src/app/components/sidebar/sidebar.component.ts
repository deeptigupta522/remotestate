import { Component, OnInit } from '@angular/core';
import { truck } from 'src/app/model/truck.model';
import { TruckService } from 'src/app/service/truck.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private truckService : TruckService) { }

  truckList: truck[]=[];
  updatetime;

  ngOnInit(): void {
    this.TruckData();
  }

  TruckData(){
    this.truckService.getData().subscribe(data => {
      this.truckList =data.data;
    })
  }

}
