export class truck{
    id?: number;
    truckNumber?: string;
    lastWaypoint?:{
        lat?:number;
        lng?: number;
        createTime?: number;
        speed?: number;
        ignitionOn?: boolean;
        updateTime?: number;
    };
    lastRunningState?:{
        stopStartTime?: number;
        truckRunningState?: number;
    }

}